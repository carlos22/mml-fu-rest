using System.Collections.Generic;
using System;
using System.Linq;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

using MyMediaLite.Data;
using MyMediaLite.ItemRecommendation;

//Entire implementation for the backend REST service 
namespace MMLItemRecommender
{
	
	//Register REST Paths
	[RestService("/rcmdr")]
	[RestService("/rcmdr/{Id}")]
	public class Rcmdr // REST Resource DTO
	{
		// user id
		public int Id { get; set; }
		public int ItemId { get; set; }
	}
	
	public class MMLApi : RestServiceBase<Rcmdr>
	{
		IncrementalItemRecommender recommender;
		
		public MMLApi(IncrementalItemRecommender rec)  {
			this.recommender = rec;
			
		}
		
		// GET: Recommend Items for User
		public override object OnGet(Rcmdr req)
		{
			if(req.Id == default(int))
				return false;
			//return MyMediaLite.ItemRecommendation.Extensions.PredictItems(recommender, req.Id, recommender.Feedback.AllItems);
			var list = new List<int>();
			list.Add(1);
			list.Add(2);
			list.Add(3);
			list.Add(4);
			
			// TODO: get all items
			IPosOnlyFeedback feed = recommender.Feedback; // why is feed null??
			IList<int> items = feed.AllItems; // WTF? -> Error CS0584: Internal compiler error: Method not found: 'MyMediaLite.Data.IDataSet.GetUsers'. (CS0584) (MML-Item-Recommender)
			
			
			return MyMediaLite.ItemRecommendation.Extensions.PredictItems(recommender, req.Id, list);
			//return MyMediaLite.Extensions.PredictItems(recommender, req.Id, list);
				
		}

		// POST: Add Feedback
		public override object OnPost(Rcmdr feedback)
		{
			recommender.AddFeedback(feedback.Id,feedback.ItemId);
			recommender.Train();
			return true;
		}
		
		
		
	}
}

