using System;
using MyMediaLite.Data;
using MyMediaLite.ItemRecommendation;

// ServiceStack
using ServiceStack.Configuration;
using ServiceStack.WebHost.Endpoints;


/**
 * HTTP REST API for a Item Recommender using MyMediaLite
 * 
 */

namespace MMLItemRecommender
{
	class MainClass
	{
		private static readonly string ListeningOn = ConfigUtils.GetAppSetting("ListeningOn");
		
		//HttpListener Hosts
		public class AppHost
			: AppHostHttpListenerBase
		{
			IncrementalItemRecommender recommender;
			public AppHost(IncrementalItemRecommender rec)
				: base("MMLItemRecommender HttpListener", typeof(HelloService).Assembly) { 
				this.recommender = rec;
			}

			public override void Configure(Funq.Container container)
			{
				container.Register(new MMLApi(recommender));
			}
		}
		
		
		public static void Main (string[] args)
		{
			
			// setup recommender
			IncrementalItemRecommender recommender = new BPRMF ();
			
			// start web server 
			var appHost = new AppHost(recommender);
			appHost.Init();
			appHost.Start(ListeningOn);

			Console.WriteLine("Started listening on: " + ListeningOn);

			Console.WriteLine("AppHost Created at {0}, listening on {1}",
				DateTime.Now, ListeningOn);


			Console.WriteLine("ReadKey()");
			Console.ReadKey();
			
			/*
			// load the data
			var training_data = ItemData.Read (args [0]);
			var test_data = ItemData.Read (args [1]);

			// set up the recommender
			var recommender = new MostPopular ();
			recommender.Feedback = training_data;
			recommender.Train ();

			// measure the accuracy on the test data set
			var results = recommender.Evaluate (test_data, training_data);
			foreach (var key in results.Keys)
				Console.WriteLine ("{0}={1}", key, results [key]);
			Console.WriteLine (results);

			// make a score prediction for a certain user and item
			Console.WriteLine (recommender.Predict (1, 1));
			*/
			
		}
		
		
	}
}
